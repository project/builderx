<?php

namespace Drupal\builderx_test\Plugin\rest\resource;

use Drupal\builderx\Builder\NodeQueryBuilder;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a resource for article entries.
 *
 * @RestResource(
 *   id = "articleTest",
 *   label = @Translation("ArticleTest apis"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/article_test"
 *   }
 * )
 */
class ArticleTestResource extends ResourceBase
{
  public function get(Request $request)
  {
    $page = $request->get('page', 1);
    $perSize = $request->get('per_size', 10);

    $builder = new NodeQueryBuilder();
    $builder->in('type', ['article']);
    $builder->title('o');
    $builder->status(1);
    $builder->duration('created', $request->get('created', ''));
    $builder->boolean($request->get('has_image', ''), function ($query) {
      /**
       * @var $query QueryInterface
       */
      $query->condition('field_image.target_id', null, 'IS NULL');
    }, function ($query) {
      /**
       * @var $query QueryInterface
       */
      $query->condition('field_image.target_id',null, 'IS NOT NULL');
    });

    $data = $builder->page($page, $perSize);
    $result = [];
    $collection = $data->getCollection()->where('nid.value', '>', 3);
    foreach ($collection as $item){
//      var_dump($item);exit();
      $result[] = ['title' => $item->title->value, 'nid' => $item->id()];
    }
    return new JsonResponse(['code' => 200, 'msg' => 'success', 'data' => [
      'page' => $data->getPage(), 'per_size' => $data->getPerSize(), 'total' => $data->getTotal(),
      'total_page' => $data->getTotalPage(), 'result' => $result
    ]]);
  }
}
