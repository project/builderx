<?php
namespace Drupal\builderx;

interface SqlDebug{
  /**
   * @return string
   */
  function debug();
}
