<?php

namespace Drupal\builderx;

interface Pager
{
  function page($page, $perSize);
}
