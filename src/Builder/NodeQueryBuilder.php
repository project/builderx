<?php

namespace Drupal\builderx\Builder;

use Drupal\builderx\Pager;
use Drupal\builderx\ParametersUtil;
use Drupal\builderx\RepositoryTrait;
use Drupal\builderx\ResultSet\PageResultSet;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 *
 * Class NodeQueryBuilder
 * @package Drupal\jpccf_restful\Utils
 */
class NodeQueryBuilder implements QueryBuilder, Pager
{
  use RepositoryTrait;

  const DATE_STATUS_ADD = 1;
  const DATE_STATUS_UPDATE = 2;

  /**
   * @var QueryInterface
   */
  protected $query;

  /**
   * NodeQueryBuilder constructor.
   * @param $type array
   * @param $status
   */
  public function __construct($type = '', $status = 1)
  {
    $this->query = $this->nodeQuery();
    $this->status($status);
    $this->types($type);
  }

  public function types($value)
  {
    return $this->in('type', $value);
  }

  public function status($value)
  {
    if (empty($value)) {
      return $this;
    }

    $this->query->condition('status', $value);
    return $this;
  }

  public function nid($value)
  {
    return $this->in('nid', $value);
  }

  public function title($title)
  {
    if (empty($title)) {
      return $this;
    }

    $this->query->condition('title', '%' . \Drupal::database()->escapeLike($title) . '%', 'LIKE');
    return $this;
  }

  public function in($field, $value)
  {
    if (empty($value)) {
      return $this;
    }

    $pairs = explode(',', $value);
    if (!empty($pairs)) {
      $this->query->condition($field, $pairs, 'IN');
    }

    return $this;
  }

  public function duration($field, $value)
  {
    if (empty($value)) {
      return $this;
    }

    $value = ParametersUtil::parseTimePara($value);

    if (!empty($value['stime'])) {
      $this->query->condition($field, $value['stime'], '>=');
    }

    if (!empty($value['etime'])) {
      $this->query->condition($field, $value['etime'], '<=');
    }

    return $this;
  }

  public function dateStatus($value = '')
  {
    if (empty($value)) {
      return $this;
    }

    //TODO changed != created when new node
    if (self::DATE_STATUS_ADD == $value) {
      $this->query->condition('changed', 'created', '<');
    } elseif (self::DATE_STATUS_UPDATE == $value) {
      $this->query->condition('changed', 'created', '>');
    }

    return $this;
  }

  public function selectList($field, $value, $all = [])
  {
    if (empty($value) || !in_array($value, $all)) {
      return $this;
    }

    $this->query->condition($field, $value);
    return $this;
  }

  public function boolean($value, $notFunc, $func)
  {
    if ($value == '') {
      return $this->query;
    }

    if ($value == 0) {
      $notFunc($this->query);
    } elseif ($value == 1) {
      $func($this->query);
    }
    return $this->query;
  }

  public function getQuery()
  {
    return $this->query;
  }

  /**
   * @param $page
   * @param $perSize
   * @return PageResultSet
   */
  function page($page, $perSize)
  {
    return new PageResultSet($page, $perSize, $this->query);
  }
}
