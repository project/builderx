<?php
namespace Drupal\builderx\Builder;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * decorate drupal's QueryInterface, then
 * clean duplicate code, keep these code "Clean, DRY, SOLID"
 * Interface QueryBuilder
 * @package Drupal\jpccf_restful\Utils
 */
interface QueryBuilder{
  /**
   *
   * @return QueryInterface
   */
  function getQuery();
}
