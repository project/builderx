<?php
namespace Drupal\builderx;

use Drupal\Core\Entity\Query\QueryInterface;
use \Drupal\Component\Plugin\Exception\PluginNotFoundException;
use \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;

/**
 * This class is shortcut for entity.
 * for example: node, taxonomy, paragraph ...
 * Trait RepositoryTrait
 */
trait RepositoryTrait{
  /**
   * @return QueryInterface
   */
  protected function nodeQuery(){
    return \Drupal::entityQuery('node');
  }

  /**
   * @return QueryInterface
   */
  protected function taxonomyQuery(){
    return \Drupal::entityQuery('taxonomy_term');
  }

  protected function taxonomyTermsByName($vid, $name){
    $terms = $this->taxonomyQuery()
      ->condition('vid', $vid)
      ->condition('name', $name)->execute();
    return $this->taxonomyLoadMultiple(array_values($terms));
  }

  /**
   * @return QueryInterface
   */
  protected function paragraphQuery(){
    return \Drupal::entityQuery('paragraph');
  }

  /**
   * @return \Drupal\Core\Entity\EntityStorageInterface
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function nodeStorage(){
    return \Drupal::entityTypeManager()->getStorage('node');
  }

  /**
   * @return EntityStorageInterfaceAlias
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function taxonomyStorage(){
    return \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  }

  /**
   * @return EntityStorageInterfaceAlias
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function paragraphStorage(){
    return \Drupal::entityTypeManager()->getStorage('paragraph');
  }

  /**
   * @param $nid
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function nodeLoad($nid){
    return $this->nodeStorage()->load($nid);
  }

  /**
   * @param $nids
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function nodeLoadMultiple($nids){
    return $this->nodeStorage()->loadMultiple($nids);
  }

  /**
   * @param $tid
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function taxonomyLoad($tid){
    return $this->taxonomyStorage()->load($tid);
  }

  /**
   * @param $tids
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function taxonomyLoadMultiple($tids){
    return $this->taxonomyStorage()->loadMultiple($tids);
  }

  /**
   * @param $pid
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function paragraphLoad($pid){
    return $this->paragraphStorage()->load($pid);
  }

  /**
   * @param $pids
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function paragraphLoadMultiple($pids){
    return $this->paragraphStorage()->loadMultiple($pids);
  }
}
