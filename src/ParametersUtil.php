<?php

namespace Drupal\builderx;

use Drupal\Component\Datetime\DateTimePlus;

class ParametersUtil
{
  /**
   * convert time string to time struct
   * @param $time 2019/09/04 - 2019/09/05
   * @return string[]
   */
  public static function parseTimePara($time)
  {
    if (empty($time)) {
      return ['stime' => null, 'etime' => null];
    }

    $timePairs = explode('-', $time);
    if (count($timePairs) >= 2) {
      return [
        'stime' => DateTimePlus::createFromFormat('Y/m/d', trim($timePairs[0]))->getTimestamp(),
        'etime' => DateTimePlus::createFromFormat('Y/m/d', trim($timePairs[1]))->getTimestamp()
      ];
    }

    return [
      'stime' => DateTimePlus::createFromFormat('Y/m/d', $timePairs[0])->getTimestamp(), 'etime' => null
    ];
  }
}
