<?php

namespace Drupal\builderx\ResultSet;

use Drupal\builderx\RepositoryTrait;
use Illuminate\Support\Collection;

class PageResultSet extends ResultSet
{
  use RepositoryTrait;

  protected $page;

  protected $perSize;

  protected $total;

  protected $totalPage;

  private $hasComputed;

  public function __construct($page, $perSize, $query)
  {
    $this->page = $page;
    $this->perSize = $perSize;
    parent::__construct($query);
  }

  /**
   * @return integer
   */
  public function getPerSize()
  {
    return $this->perSize;
  }

  /**
   * @return integer
   */
  public function getPage()
  {
    return $this->page;
  }

  /**
   * @return integer
   */
  public function getTotal()
  {
    $this->lazyCompute();
    return $this->total;
  }

  /**
   * @return integer
   */
  public function getTotalPage()
  {
    $this->lazyCompute();
    return $this->totalPage;
  }

  /**
   * @return Collection
   */
  public function getCollection()
  {
    $this->lazyCompute();
    return parent::getCollection();
  }

  protected function lazyCompute()
  {
    if (!$this->hasComputed) {
      $this->total = (int)(clone $this->query)->count()->execute();
      $pageIndex = $this->page > 0 ? ($this->page - 1) * $this->perSize : 0;
      $elements = $this->query->range($pageIndex, $this->perSize)->execute();
      $this->collection = collect($this->nodeLoadMultiple(array_values($elements)));
      $this->totalPage = (int)($this->perSize) == 0 ? 1 : ceil($this->total / $this->perSize);
      $this->hasComputed = true;
    }
  }

  public function __toString()
  {
    $count = count($this->collection);
    $page = $this->getPage();
    $total = $this->getTotal();
    $totalPage = $this->getTotalPage();
    $perSize = $this->getPerSize();
    return "Page: $page, Total: $total, TotalPage: $totalPage, PerSize: $perSize, Count: $count";
  }
}
