<?php

namespace Drupal\builderx\ResultSet;

use Drupal\builderx\SqlDebug;
use Drupal\Core\Entity\Query\QueryInterface;

class ResultSet implements SqlDebug
{
  /**
   * @var QueryInterface
   */
  protected $query;

  protected $collection;

  public function __construct($query)
  {
    $this->query = $query;
  }

  /**
   * @return mixed
   */
  public function getCollection()
  {
    return $this->collection;
  }

  public function debug()
  {
    return strval($this->query);
  }
}
